import React from 'react';
import ReactDOM from 'react-dom';
import App from './Containers/App';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { reducers, actions } from './Redux/index';
import createHistory from 'history/createBrowserHistory';
import { Router } from 'react-router-dom'
import createSagaMiddleware from 'redux-saga';
import sagas from './Sagas/index';

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(combineReducers(reducers), composeEnhancers(applyMiddleware(sagaMiddleware)));

sagas.forEach((saga) => sagaMiddleware.run(saga));

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

store.dispatch(actions.fetchUsersStart());

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>,
    document.getElementById('root'));











/*



// basics of generators:
function* generator() {
    yield "bar"
    console.log("foo");
    return "baz"
}

const iterator = generator();

console.log("not called yet");
let next = iterator.next();
console.log(next);
next = iterator.next();
console.log(next);


console.log("-------------------------------");

// passing values into generators and receiving with yield:
function* gen2() {
    const valuePassedIntoIterator = yield "waiting...";
    return valuePassedIntoIterator;
}
const it2 = gen2();
console.log(it2.next());
console.log(it2.next("boo"));

console.log("-------------------------------");

// using for...of to iterate
function* gen3() {
    yield "foo"
    yield "bar"
    yield "baz"
    return "boo"
}

const it3 = gen3();
let yielded;
for (yielded of it3) {
    console.log(yielded);
}

console.log("-------------------------------");

// delegating yield with "yield*" (*mindfuck*)

function* gen4a() {
    yield "A"
    yield "B"
    yield* gen4b();
    yield "E"
}
function* gen4b() {
    yield "C"
    yield "D"
    return "this is thrown away"
}

const it4 = gen4a();

for (let yielded4 of it4) {
    console.log(yielded4);
}

console.log("-------------------------------");

// throwing from generators

function* gen5() {
    while (true) {
        try {
            console.log(yield);
        } catch (e) {
            console.error("caught: ", e);
        }
    }
}

const it5 = gen5();
it5.next("ignored");
it5.throw("foo");
it5.throw("bar");
it5.next(2);

console.log("-------------------------------");


// Now, let's look at how generators can be used to fetch data...

function fetchUser(userId, callback) {

    console.log("5 - fetchuser called");

    fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .then(response => {
            if (response.status < 300) {
                response.json().then(data => {

                    console.log("6 - called callback from fetchUser with user", callback, data);

                    callback(data);
                });
            } else {
                callback(response);
            }
        });
}

const call = (method, ...args) => {
    
    console.log("3 - created curried function");
    
    return (callback) => {

        console.log("4 - calling method passed to call");

        args.push(callback);
 
        return method(...args);
    };
};
 
const controller = (generator) => {
    const iterator = generator();

    console.log("1 - created iterator from generator");
 
    const advancer = (response) => {

        console.log("2 - advancer called with response", response);

        if (response && response.ok === false) {
            return iterator.throw(response);
        }

        let state = iterator.next(response);
 
        if (!state.done) {
            state.value(advancer);
        }
    }
 
    advancer();
};

const generator = function * () {
    let user1, user2, user99;

    try {
        user1 = yield call(fetchUser, 1);
        user2 = yield call(fetchUser, 2);
        user99 = yield call(fetchUser, 99);
    } catch (e) {
        console.error("caught: ",e);
    }

    console.log(user1, user2, user99);
}
 
controller(generator);



*/