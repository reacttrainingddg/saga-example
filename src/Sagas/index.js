import { takeLatest, put, call } from 'redux-saga/effects';
import { actions, actionTypes } from '../Redux/index';

function * fetchUsersSaga() {
    yield takeLatest(actionTypes.FETCH_USERS_START, function * () {
        try {
            const users = yield call(() => fetch("https://jsonplaceholder.typicode.com/users").then(resp => resp.json()));
            yield put(actions.fetchUsersSuccess(users));
        } catch (e) {
            yield put(actions.fetchUsersFailure("damn :("));
        }
    })
}

export default [
    fetchUsersSaga
]