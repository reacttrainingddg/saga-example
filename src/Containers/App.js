import React, { Component } from 'react'
import Sidebar from '../Components/Sidebar'
import UserDetails from '../Components/UserDetails'
import UserList from '../Containers/UserList'
import Main from '../Components/Main'
import HomeScreen from '../Components/HomeScreen'
import { connect } from 'react-redux'
import { Route, Link, withRouter } from 'react-router-dom';

const mapStateToProps = (state) => ({
    users: state.Data.users
});

class App extends Component {

    render() {
        return (
            <React.Fragment>
                <h1>
                    <Link to="/">HOME</Link>
                </h1>
                <div style={{
                    display: "flex",
                    alignItems: "stretch"
                }}>
                    <Sidebar>
                        <UserList />
                    </Sidebar>
                    <Main>
                        <Route path="/users/:userId" render={route => {
                            const userId = route.match.params.userId;
                            return <UserDetails user={this.props.users.items.find(user => parseInt(user.id, 10) === parseInt(userId, 10))} />
                        }} />
                        <Route exact path="/" component={HomeScreen} />
                    </Main>
                </div>
            </React.Fragment>
        )
    }
}

// !!! we need withRouter() here, otherwise no actions are triggered! see https://reacttraining.com/react-router/web/guides/redux-integration
export default withRouter(connect(mapStateToProps)(App));
