import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import { createMockStore } from 'redux-test-utils';
import ConnectedUserList from './UserList';

configure({ adapter: new Adapter() });

const shallowWithStore = (component, store) => {
	const context = {
		store,
	};
	return shallow(component, { context });
};

 const testState = {
     Data: {
         users: {
             fetching: false,
             errorMessage: "",
             items: [
				 {
					 id: 1,
					 name: "Leanne Graham",
					 username: "Bret",
					 email: "Sincere@april.biz",
				 },
				 {
					 id: 2,
					 name: "Foo Bar",
					 username: "Bret",
					 email: "Sincere@april.biz",
				 },
             ]
         }
     }
 };

const store = createMockStore(testState);

describe("test the output without testing real markup", () => {
	it("renders the correct number of users", () => {

		const wrapper = shallowWithStore(<ConnectedUserList />, store);

		// -> :(
		// we have to work around the HOC created by connect() !
		// expect(wrapper.find('UserListItem').length).toEqual(2);

		expect(wrapper.find('UserList').length).toEqual(1);
		expect(wrapper.find('UserList').dive().find('UserListItem').length).toEqual(2);
	})


	it("passes the users to UserListItem correctly", () => {

		const wrapper = shallowWithStore(<ConnectedUserList />, store);

		expect(wrapper.find('UserList').dive().find({ user: testState.Data.users.items[0] }).length).toEqual(1);
	})
})
