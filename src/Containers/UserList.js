import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import UserListItem from '../Components/UserListItem'
import { connect } from 'react-redux'
import { selectors } from '../Redux/index';


const mapStateToProps = (state) => ({
    users: state.Data.users,
    numberOfUsers: selectors.numberOfUsers(state),
    numberOfUsersIsEven: selectors.numberOfUsersIsEven(state),
    sumOfUsersIds: selectors.sumOfUsersIds(state),
    sumOfUserIdsMinusNumberOfUsers: selectors.sumOfUserIdsMinusNumberOfUsers(state)
});

class UserList extends PureComponent {

    static propTypes = {
        users: PropTypes.object.isRequired
    }

    render() {
        return (
            <React.Fragment>
                <h1>{this.props.numberOfUsers} Users</h1>
                <p><small>(That's an {this.props.numberOfUsersIsEven ? "even" : "odd"} number)</small></p>
                <p><small>Sum of user ids is: {this.props.sumOfUsersIds}</small></p>
                <p><small>Sum of user ids minus number of users:: {this.props.sumOfUserIdsMinusNumberOfUsers}</small></p>
                {
                    this.props.users.items.length ?
                        this.props.users.items.map(user =>
                            <UserListItem key={user.id} user={user} />)
                        : this.props.users.fetching ? 
                            "Lade User..." :
                            <span style={{color: "red"}}>{this.props.users.errorMessage}</span>
                }
            </React.Fragment>
        )
    }
}

export default connect(mapStateToProps)(UserList);