import { createActions, handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import { createSelector } from 'reselect';


// ACTION TYPES -------------------------------------------------------------
const FETCH_USERS_START = "FETCH_USERS_START";
const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
const FETCH_USERS_FAILURE = "FETCH_USERS_FAILURE";

export const actionTypes = {
    FETCH_USERS_START,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
}

// ACTIONS      -------------------------------------------------------------
export const actions = createActions({
    FETCH_USERS_START : () => {},
    FETCH_USERS_SUCCESS: users => ( { users } ),
    FETCH_USERS_FAILURE: errorMessage => ( { errorMessage } )
});


// REDUCERS     -------------------------------------------------------------

const usersReducer = handleActions({
    [FETCH_USERS_START]: (state, action) => Object.assign({}, state, { fetching: true }),
    [FETCH_USERS_SUCCESS]: (state, action) => Object.assign({}, state, { fetching: false, items: action.payload.users }),
    [FETCH_USERS_FAILURE]: (state, action) => Object.assign({}, state, { fetching: false, errorMessage: action.payload.errorMessage })
}, { items: [], fetching: false, errorMessage: '' })

const dataReducer = combineReducers({
    users: usersReducer,
    posts: (state = {}, action) => state
});

export const reducers = {
    Data: dataReducer,
    Ui: (state = {}, action) => state
};

// issue here: the reducers only get the part of state they're "responsible" for. if a reducer needs to update
// other parts of the state, it cannot do so without remodeling the state. combineReducers can be replaced
// with a custom version that does not partition the state, but passes the full state to each reducer.


// SELECTORS    -------------------------------------------------------------
// selectors are functions that read a part of the state in a repeatable and composable way.
// They can perform calculations based on the state that return a semantic result.

// a selector can be a function that simply reads from the state:
const userList = state => state.Data.users.items;
const numberOfUsers = state => state.Data.users.items.length;

// selectors can also do more complex things:
const numberOfUsersIsEven = state => state.Data.users.items.length % 2 === 0;

// it makes sense to combine these selectors:
const numberOfUsersCombined = state => userList(state).length;
const numberOfUsersIsEvenCombined = state => numberOfUsersCombined(state) % 2 === 0;

// as the calculations get more complex, performance goes down, especially if the same value
// is read from the state in several different places. 
// reSelect solves this issue by creating memoized selectors and makes selector combination more elegant and declarative.
const numberOfUsersReselected = createSelector(
    userList,
    items => items.length
)

const sumOfUsersIds = createSelector(
    userList,
    items => items.reduce((prev, cur) => { return prev + cur.id }, 0)
)

// this is more declarative than chained calls
const sumOfUserIdsMinusNumberOfUsers = createSelector(
    [ numberOfUsersReselected, sumOfUsersIds ], // can also be passed as multiple args, but this is more clear imho
    (numberOfUsers, sumOfUserIds) => sumOfUserIds - numberOfUsers
)


export const selectors = {
    numberOfUsers: numberOfUsersReselected,
    numberOfUsersIsEven: numberOfUsersIsEvenCombined,
    sumOfUsersIds,
    sumOfUserIdsMinusNumberOfUsers
}
