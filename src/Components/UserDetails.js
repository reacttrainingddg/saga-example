import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import googleMapsApi from 'google-maps-api'


export default class UserDetails extends PureComponent {

    static propTypes = {
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            username: PropTypes.string.isRequired,
        })
    }

    createMap = () => {
        googleMapsApi("AIzaSyAPkRazRG7epDKUNLOb8s75cOiOzkauomQ")().then(maps => {

            const usersLocation = {
                lat: parseFloat(this.props.user.address.geo.lat),
                lng: parseFloat(this.props.user.address.geo.lng),
            }

            this.map = new maps.Map(this.mapNode, {
                center: usersLocation,
                zoom: 5
            });

            new maps.Marker({
                position: usersLocation,
                map: this.map,
                title: this.props.user.name
            });
        });
    }

    componentDidMount() {
        this.createMap();
    }

    componentWillReceiveProps() {
        this.createMap();
    }

    render() {
        if(!this.props.user) {
            return <div>Warte auf Daten...</div>
        }
        return (
            <div>
                <h1>{this.props.user.username}</h1>
                <p>{this.props.user.name}</p>
                <p>{this.props.user.email}</p>
                <p>Lat: {this.props.user.address.geo.lat} Lng: {this.props.user.address.geo.lng}</p>
                <div ref={node => { this.mapNode = node }} style={{ height: 400, width: "100%" }}></div>
            </div>
        )
    }
}
