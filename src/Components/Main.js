import React, { PureComponent } from 'react'

export default class Main extends PureComponent {

    render() {
        return (
            <div style={{
                flexBasis: "70vw"
                }}>
                {this.props.children}
            </div>
        )
    }
}
