import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import UserDetails from './UserDetails';

const user = {
	id: 1,
	name: "Leanne Graham",
	username: "Bret",
	email: "Sincere@april.biz",
	address: {
		geo: {
			lat: 38.255,
			lng: 9.555
		}
	}
};

storiesOf('UserDetails', module)
	.add('with user', () => <UserDetails user={user} />);
