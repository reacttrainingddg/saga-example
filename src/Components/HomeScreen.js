import React, { PureComponent } from 'react'

export default class HomeScreen extends PureComponent {

    render() {
        return (
            <div>
                <h1>Welcome</h1>
                <p>No user selected. Select one on the left!</p>
                {this.props.foo}
            </div>
        )
    }
}
