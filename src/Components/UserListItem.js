import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

export default class UserListItem extends PureComponent {

    static propTypes = {
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            username: PropTypes.string.isRequired,
        }).isRequired
    }

    render() {
        return (
            <Link to={`/users/${this.props.user.id}`}>
                <p>
                    <small>{this.props.user.id}</small>
                    <strong>{this.props.user.username}</strong>
                </p>
                <p>{this.props.user.name}</p>
                <hr />
            </Link>
        )
    }
}
