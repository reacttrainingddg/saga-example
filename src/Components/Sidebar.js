import React, { PureComponent } from 'react'

export default class Sidebar extends PureComponent {

    render() {
        return (
            <div style={{
                flexBasis: "30vw"
                }}>
                {this.props.children}
            </div>
        )
    }
}
